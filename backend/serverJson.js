const fs = require('fs');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 1234;

// Middleware to parse JSON data
app.use(bodyParser.json());
app.use(cors({
  origin: 'http://localhost:3000'
}));

// JSON frame data
let userData = {
  "username": "Benoit Coeugnet",
  "email": "test@test.fr",
  "locale": "en-US",
  "assoSuivies": [
    {
      "nomAsso": "AS Taekwondo",
      "assoId": 0,
      "lien": "/association/astaekwondo"
    },
    {
      "nomAsso": "AS Karaté",
      "assoId": 1,
      "lien": "/association/askarate"
    },
    {
      "nomAsso": "AS Judo",
      "assoId": 2,
      "lien": "/association/asjudo"
    },
    {
      "nomAsso": "AS Tennis",
      "assoId": 3,
      "lien": "/association/astennis"
    }
  ],
  "rdv": [
    {
      "title": "Website Re-Design Plan",
      "startDate": "2023-6-6 11:30:00",
      "endDate": "2023-6-6 14:30:00",
      "id": 0
    },
    {
      "title": "Book Flights to San Fran for Sales Trip",
      "startDate": "2023-6-6 11:30:00",
      "endDate": "2023-6-6 14:30:00",
      "id": 1
    }
  ],
  "info": [
    {
      "title": "Select campaign settings",
      "text": "For each ad campaign that you create, you can control how much you're willing to spend on clicks and conversions, which networks and geographical locations you want your ads to show on, and more.",
      "author": "",
      "link": ""
    },
    {
      "title": "Create an ad group",
      "text": "An ad group contains one or more ads which target a shared set of keywords.",
      "author": "",
      "link": ""
    },
    {
      "title": "Create an ad",
      "text": "Try out different ad text to see what brings in the most customers, and learn how to enhance your ads using features like ad extensions. If you run into any problems with your ads, find out how to tell if they're running and how to resolve approval issues.",
      "author": "",
      "link": ""
    }
  ]
};
const userInfoFilePath = path.join(__dirname, '../src/data/userInfo.json');
const assoInfoFilePath = path.join(__dirname, '../src/data/assoInfo.json');

// POST route to store new data
app.post('/users', (req, res) => {
  console.log('----------------------------------------');
  console.log('Received data:', req.body);

  const newData = req.body;

  // Update the JSON frame with the new data
  userData = Object.assign(userData, newData);
  console.log(`Json ` + JSON.stringify(userData));
  fs.writeFileSync(userInfoFilePath, JSON.stringify(userData));

  res.status(200).json({ message: 'Data stored successfully' });
});

app.post('/asso', (req, res) => {
  console.log('----------------------------------------');
  console.log('Received data:', req.body);
  const newData = req.body;
  const { name } = req.query;
  const fileData = fs.readFileSync(assoInfoFilePath, 'utf8');
  const data = JSON.parse(fileData);
  data[name]=newData;

  // Update the JSON frame with the new data
  //userData = Object.assign(userData, newData);
  console.log(`Json ` + JSON.stringify(data));
  fs.writeFileSync(assoInfoFilePath, JSON.stringify(data));
  res.status(200).json({ message: 'Data stored successfully' });
});

app.get('/assos', (req, res) => {
  console.log(req.query);
  const { name } = req.query;
  const fileData = fs.readFileSync(assoInfoFilePath, 'utf8');
  const data = JSON.parse(fileData);

  res.status(200).json(data[name]);
});

app.get('/test', (req, res) => {
  console.log(req.query);
  const { name } = req.query;
  const fileData = fs.readFileSync(assoInfoFilePath, 'utf8');
  const data = JSON.parse(fileData);

  res.status(200).json(data[name]);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
