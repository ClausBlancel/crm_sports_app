import React from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Unstable_Grid2";
import { Paper, Typography, Grow, Box, Container } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import AppCSS from "../App.css";

import AssosPng from "./Assos.png";
import AssosDark from "./AssosDark.png";

import useTheme from "@mui/material/styles/useTheme";
import "@fontsource/inter";

import GroupsIcon from "@mui/icons-material/Groups";
import MailIcon from "@mui/icons-material/Mail";
import EditCalendarIcon from "@mui/icons-material/EditCalendar";
import EuroIcon from "@mui/icons-material/Euro";
import SportsKabaddiIcon from "@mui/icons-material/SportsKabaddi";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import MonitorWeightIcon from "@mui/icons-material/MonitorWeight";
import TerminalIcon from "@mui/icons-material/Terminal";
import Diversity1Icon from '@mui/icons-material/Diversity1';


const iconsFunctionalities = [
  GroupsIcon,
  MailIcon,
  EditCalendarIcon,
  EuroIcon,
  SportsKabaddiIcon,
  AccountBalanceIcon,
  MonitorWeightIcon,
  TerminalIcon,
];

const textFunctionalities = [
  "Gestion des adhérants",
  "Messagerie de club",
  "Gestion des évènements",
  "Gestion des cotisations",
  "Gestion des compétitions",
  "Gestion du bugdet",
  "Gestion des catégories",
  "Gestion du club",
];

function Home() {
  const theme = useTheme();

  const Item = styled(Paper)(() => ({
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.primary,
  }));

  const typoProperties = {
    mr: 2,
    display: { xs: "flex", md: "flex" },
    fontFamily: "Inter",
    fontWeight: 500,
    letterSpacing: ".001rem",
    color: theme.palette.text.primary,
    textDecoration: "none",
  };

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ backgroundColor: theme.palette.background.secondary }}>
        <Container maxWidth="lg">
          <Grid container spacing={0}>
            <Grid item xs={6} style={{ padding: "60px 0px" }}>
              <Grid xs display="flex">
                <Grid item>
                  <Grow
                    style={{ transformOrigin: "0 0 0" }}
                    in={true}
                    timeout={2000}
                  >
                    <Typography
                      variant="h1"
                      noWrap
                      sx={{
                        display: { xs: "flex", md: "flex" },
                        fontFamily: "Inter",
                        fontWeight: 1000,
                        fontSize: "3.5rem",
                        letterSpacing: ".001rem",
                        color: theme.palette.text.primary,
                        textDecoration: "none",
                      }}
                    >
                      Surpassez-vous.
                    </Typography>
                  </Grow>
                  <Grow
                    style={{ transformOrigin: "0 0 0" }}
                    in={true}
                    timeout={2000}
                  >
                    <Typography
                      variant="h4"
                      noWrap
                      sx={{
                        display: { xs: "flex", md: "flex" },
                        fontFamily: "Inter",
                        fontWeight: 300,
                        fontSize: "1rem",
                        letterSpacing: ".001rem",
                        color: theme.palette.text.primary,
                        textDecoration: "none",
                      }}
                    >
                      Gérez votre club de sport comme un professionnel.
                    </Typography>

                  </Grow>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6} sx={{ mb: 4 }}>
              <Grow
                style={{ transformOrigin: "0 0 0" }}
                in={true}
                timeout={2000}
              >
                {theme.palette.mode === "light" ? (

                  <img
                    class="responsive"
                    src={AssosPng}
                    alt="Logo"
                  />
                ) : (
                  <img
                    class="responsive"
                    src={AssosDark}
                    alt="Logo"
                  />
                )}

              </Grow>
            </Grid>
          </Grid>
        </Container>
      </Box>

      <Container maxWidth="md">
        <Grid container spacing={0}>
          <Grid item xs={6} style={{ padding: "100px 0px" }}>
            <Grid>
              <Grow
                style={{ transformOrigin: "0 0 0" }}
                in={true}
                timeout={2000}
              >
                <Typography variant="subtitle1" sx={typoProperties}>
                  Sans cesse, nous devons nous surpasser, car c'est maintenant
                  que la maîtrise du sport est la plus importante. C'est
                  maintenant que nous devons nous dépasser et c'est maintenant
                  que nous devons nous surpasser. Prenez en mains votre futur
                  et devenez le meilleur. <br />
                  Avec CRM Sport, vous pourrez gérer votre club de sport comme
                  un professionnel.
                </Typography>
              </Grow>
            </Grid>
          </Grid>
          <Grid item xs={6} style={{ padding: "100px 0px" }}>
            <Grow
              style={{ transformOrigin: "0 0 0" }}
              in={true}
              timeout={2000}
            >
              <Diversity1Icon sx={{ fontSize: 653, height: 216, color: theme.palette.text.primary }} />
            </Grow>
          </Grid>

        </Grid>

        <Box sx={{ flexGrow: 1 }} style={{ padding: "30px 0px" }}>
          <Grow style={{ transformOrigin: "0 0 0" }} in={true} timeout={2000}>
            <Grid item xs={12} style={{ padding: "30px 130px" }}>
              <Typography
                variant="h4"
                sx={typoProperties}
                justifyContent={"center"}
              >
                Nos fonctionnalités
              </Typography>
            </Grid>
          </Grow>
          <Grow style={{ transformOrigin: "0 0 0" }} in={true} timeout={2000}>
            <Grid
              container
              spacing={{ xs: 1, md: 3 }}
              columns={{ xs: 1, sm: 4, md: 16 }}
            >
              {iconsFunctionalities.map((Icon, index) => (
                <Grid xs={1} sm={4} md={4} key={index}>
                  <Item>
                    <Icon key={index} sx={{ fontSize: 40 }} />
                    <Typography
                      variant="subtitle1"
                      sx={{
                        fontFamily: "Inter",
                        fontWeight: 500,
                        letterSpacing: ".001rem",
                        color: theme.palette.text.primary,
                        textDecoration: "none",
                      }}
                    >
                      {textFunctionalities[index]}
                    </Typography>
                  </Item>
                </Grid>
              ))}
            </Grid>
          </Grow>
        </Box>
      </Container>
    </ThemeProvider >
  );
}

export default Home;
