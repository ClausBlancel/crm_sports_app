import * as React from 'react';
import Pricing from '../components/Pricing/Pricing';
import { Box } from '@mui/material';

function PagePricing() {
    return (
        <Box >
            <Pricing />
        </Box >
    );
}

export default PagePricing;
