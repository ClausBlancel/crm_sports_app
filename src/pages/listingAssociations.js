import React, { useState } from "react";
import { Link, Router } from "react-router-dom";
import associations from "./../data/associationList.json";
import { Typography, Box, Button, Select, MenuItem, useTheme } from "@mui/material";
import "./asso.css";
import Grid from "@mui/material/Unstable_Grid2";
import SortByAlphaOutlinedIcon from "@mui/icons-material/SortByAlphaOutlined";


import ReorderRoundedIcon from '@mui/icons-material/ReorderRounded';
import AppsRoundedIcon from '@mui/icons-material/AppsRounded';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import FilterAltIcon from '@mui/icons-material/FilterAlt';


const ListingAssociations = () => {
  const theme = useTheme();

  const [search, setSearch] = useState("");
  const [assos, setAssos] = useState(associations);
  const [aToZ, setaToZ] = useState(true);
  const [gridDisplay, setGridDisplay] = useState(true);
  const [type, setType] = React.useState("Tout");
  const searchChange = (e) => {
    const searchValue = e.target.value.toLowerCase();
    setSearch(searchValue);
  };
  const handleTypeFilter = (event) => {
    setType(event.target.value);
  };
  const handleDisplay = (event) => {
    setGridDisplay(!gridDisplay);
  };
  const handleOrder = () => {
    setaToZ(!aToZ);
  };


  const filteredAssos = assos.filter((asso) => {
    if (type === "Tout") {
      return asso["nomAsso"].toLowerCase().includes(search);
    } else {
      return (
        asso["nomAsso"].toLowerCase().includes(search) &&
        asso["type"] === type
      );
    }
  }).sort((a, b) => {
    return aToZ
      ? a["nomAsso"].toLowerCase() > b["nomAsso"].toLowerCase()
        ? 1
        : -1
      : a["nomAsso"].toLowerCase() > b["nomAsso"].toLowerCase()
        ? -1
        : 1;
  });
  return (
    <>

      <div className="search">
        <Typography align='center' style={{
          color: theme.palette.text.primary,
          width: "max-content",
          fontSize: 25,
          fontFamily: "Inter",
          fontWeight: "bold",
        }}
        >Les assos </Typography>
      </div>
      <div className="search">
        <input
          type="search"
          placeholder="Chercher association"
          onChange={searchChange}
          backgroundColor={theme.palette.text.primary}
        /><SearchRoundedIcon sx={{ color: theme.palette.text.primary }} />
      </div>
      <div className="search">
        <Button onClick={handleOrder}>
          <SortByAlphaOutlinedIcon sx={{ color: theme.palette.text.primary }} />
        </Button>
        <>
          <FilterAltIcon sx={{ color: theme.palette.text.primary }} />
          <Select value={type} label="Type" onChange={handleTypeFilter}>
            <MenuItem value={"Tout"}>Tout</MenuItem>
            <MenuItem value={"Sport de combat"}>Sport de combat</MenuItem>
            <MenuItem value={"Sport collectif"}>Sport collectif</MenuItem>
            <MenuItem value={"Sport individuel"}>Sport individuel</MenuItem>
            <MenuItem value={"Sport de raquette"}>Sport de raquette</MenuItem>
          </Select>
        </>
        <Button onClick={handleDisplay}>
          {gridDisplay ?
            (<ReorderRoundedIcon sx={{ color: theme.palette.text.primary }} />) :
            (<AppsRoundedIcon sx={{ color: theme.palette.text.primary }} />)}
        </Button>
      </div>
      <div className={gridDisplay ? ("as-list-grid") : ("as-list")}>
        {filteredAssos.map((asso, index) => {
          return (
            <Box className="as-container" align="center" sx={{ backgroundColor: theme.palette.background.secondary, border: "2px solid" + theme.palette.general.main }}>
              <Grid item xs={3}>
                <Typography align='center' style={{
                  color: theme.palette.text.primary,
                  width: "max-content",
                  fontSize: 25,
                  fontFamily: "Inter",
                  fontWeight: "bold",
                  justifySelf: "center",
                  textAlign: "center"
                }}
                >{asso["nomAsso"]}</Typography>
                {asso["type"] !== "null" ? (
                  <Grid item xs={3}>
                    <Box className="type" sx={{ backgroundColor: theme.palette.background.tertiary, border: "1px solid" + theme.palette.general.main }}>
                      <p className="type-text" style={{ color: theme.palette.text.secondary }}>{asso["type"]}</p>
                    </Box>
                  </Grid>
                ) : null}
              </Grid>
              <p className="as-desc" style={{ color: theme.palette.text.secondary }}>{asso["description"]}</p>
              <Box >
                <Button variant='outlined' component={Link} to={asso["lien"]} sx={{
                  border: 1,
                  margin: 1,
                  fontFamily: "Inter",
                  backgroundColor: theme.palette.background.tertiary,
                  color: theme.palette.text.primary,
                  display: 'flex',
                  flexDirection: 'column',
                  width: 'max-content'
                }}>
                  Voir plus
                </Button>
              </Box>
            </Box>
          );
        })}
      </div>
    </>
  );
};
export default ListingAssociations;
