import React, { useState, useRef, useEffect } from "react";
import { useParams } from 'react-router';
import moment from "moment";
import axios from "axios";
import {
  List,
  ListItem,
  ListItemText,
  useTheme
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

//Calendrier
import { ViewState, EditingState, IntegratedEditing } from "@devexpress/dx-react-scheduler";
import {
  Scheduler,
  DayView,
  WeekView,
  Appointments,
  DateNavigator,
  TodayButton,
  Toolbar,
  ViewSwitcher,
  AppointmentForm,
  AppointmentTooltip,
  ConfirmationDialog,
} from "@devexpress/dx-react-scheduler-material-ui";
import Paper from "@mui/material/Paper";

//news
import MobileStepper from "@mui/material/MobileStepper";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import ManageAccountsOutlinedIcon from '@mui/icons-material/ManageAccountsOutlined';

import user from "./../data/userInfo.json";
import asso from "./../data/assoInfo.json";

const locale = user["locale"];
const localizationMessages = {
  "fr-FR": {
    "today": "Aujourd'hui",
    "week": "Semaine",
    "day": "Jour",
    "next": "Suivant",
    "back": "Précédent"
  },
  "en-US": {
    "today": "today",
    "week": "Week",
    "day": "Day",
    "next": "Next",
    "back": "Back"
  }
};


function CalendarDisplay() {
  const theme = useTheme();

  const getTodayMessages = (locale) => localizationMessages[locale];
  const getWeekMessages = (locale) => localizationMessages[locale]["week"];
  const getDayMessages = (locale) => localizationMessages[locale]["day"];
  function commitChanges(){

  };
  return (
    <>
      <h2 style={{ color: theme.palette.text.primary }}>Calendar</h2>
      <Paper>
        <Scheduler
          data={user["rdv"]}
          locale={locale}
          height={600}
          adaptivityEnabled={true}
          color={theme.palette.general.main}
        >
          <ViewState defaultCurrentViewName={getWeekMessages(locale)} />
          <EditingState
            onCommitChanges={commitChanges}
          />
          <IntegratedEditing />
          <DayView
            name={getDayMessages(locale)}
            startDayHour={0}
            endDayHour={24}
          />
          <WeekView
            name={getWeekMessages(locale)}
            cellDuration={60}
            startDayHour={8}
            endDayHour={18}
          />
          <Toolbar />
          <ViewSwitcher />
          <DateNavigator />
          <TodayButton messages={getTodayMessages(locale)} />
          <ConfirmationDialog />
          <Appointments />
          <AppointmentTooltip
            showOpenButton
            showDeleteButton
          />
          <AppointmentForm  />
        </Scheduler>
      </Paper>
    </>
  );
}

function MemberDisplay( props) {
  const theme = useTheme();
  const Members = props.asso["Members"]

  const listStyle = {
    width: "max-content",
    maxWidth: 360,
    bgcolor: theme.palette.background.secondary,
    color: theme.palette.text.secondary
  };

  return (
    <>
      <h2 style={{ color: theme.palette.text.primary }}>Members</h2>
      <List sx={listStyle}>
        {Members.map((member) => (
          <>
            <ListItem>
              {member.type==="admin" ?(
              <><ManageAccountsOutlinedIcon />
              <ListItemText primary={member.name} secondary="admin" secondaryTypographyProps={{fontSize:11,textAlign:"left"}} />
              </>) :
              (<><AccountCircleIcon />
              <ListItemText primary={member.name} /></>)}
              

            </ListItem>
          </>
        ))}
      </List>
    </>
  );
}

function NewsDisplay( props) {
  const theme = useTheme();
  const adminRights = props.adminRights;

  const steps = props.asso["info"];
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = steps.length;

  const newStyle = {
    padding: "10px",
    backgroundColor: theme.palette.background.secondary,
    color: theme.palette.text.secondary
  };

  const news = [
    { title: "News 1", content: "News 1 content" },
    { title: "News 2", content: "News 2 content" }
  ];

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
      setActiveStep(0);
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    if (activeStep === 0) {
      setActiveStep(steps.length - 1);
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  };

  const [open, setOpen] = React.useState(false);
  const newsTitle = useRef();
  const newsDesc = useRef();
  const handleNewNewsOpen = () => {
    setOpen(true);
  };

  const handleNewNewsClose = () => {
    setOpen(false);
  };
  const handleNewNews = () => {
    //Send newsTitle et newsDesc
    
    const data={
      "title":newsTitle.current.value,
      "text":newsDesc.current.value
    }
    props.asso['info'].push(data);

    axios
      .post('http://localhost:1234/asso?name='+props.name, props.asso) // Send GET request to server
      .then((res) => {
        
      })
      .catch((err) => console.log(err));
    console.log(newsTitle.current.value)
    //const user = JSON.stringify(data);

    
    setOpen(false);
  };

  return (
    <>
      <h2 style={{ color: theme.palette.text.primary }}>News</h2>
      <Box borderColor={theme.palette.text.primary} sx={{ maxWidth: 1440, flexGrow: 0 }}>
        <Paper
          square
          elevation={2}
          sx={{
            display: "flex",
            alignItems: "center",
            height: 50,
            pl: 2,
            bgcolor: theme.palette.background.secondary
          }}
        >
          <Typography
            sx={{ fontFamily: "Inter", color: theme.palette.text.secondary }}
          >
            {steps[activeStep]["title"]}
          </Typography>
        </Paper>
        <Box
          component="div"
          sx={{
            height: 100,
            maxWidth: 1440,
            p: 2,
            overflowY: "auto",
            fontFamily: "Inter",
            border: 2,
            color: theme.palette.text.secondary,
            borderColor: theme.palette.background.secondary,
            bgcolor: theme.palette.background.secondary
          }}
        >
          {steps[activeStep]["text"]}
        </Box>
        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          sx={{ bgcolor: theme.palette.background.secondary }}
          nextButton={
            <Button
              sx={{ color: theme.palette.text.secondary, fontFamily: "Inter" }}
              size="small"
              onClick={handleNext}
            >
              {localizationMessages[locale]["next"]}
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              sx={{ color: theme.palette.text.secondary, fontFamily: "Inter" }}
              size="small"
              onClick={handleBack}
            >
              <KeyboardArrowLeft />
              {localizationMessages[locale]["back"]}
            </Button>
          }
        />
      </Box>
      { adminRights ? (<Box sx={{ margin: 1 }} align='end'>
        <Button variant='outlined' sx={{ borderColor: theme.palette.general.main, color: theme.palette.text.primary }} onClick={handleNewNewsOpen} startIcon={<ModeEditOutlineOutlinedIcon sx={{ color: theme.palette.general.main }} />}>Add News</Button>
      </Box>):(<div></div>)}
      
      <Dialog open={open} onClose={handleNewNewsClose}>
        <DialogTitle>New News</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter the title of the news and your message
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="newsTitle"
            label="News title"
            type="email"
            fullWidth
            variant="filled"
            inputRef={newsTitle}
          />
          
          <TextField
            autoFocus
            margin="dense"
            id="news"
            label="News"
            type="email"
            fullWidth
            variant="outlined"
            multiline
            rows={4}
            inputRef={newsDesc}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleNewNewsClose}>Cancel</Button>
          <Button onClick={handleNewNews}>Upload</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

function PageAsso(profile) {
  const theme = useTheme();
  const { name}=useParams();
  const [adminRights, setAdminRights] = useState(false);
  const [asso, setAsso] = useState();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    axios
      .get('http://localhost:1234/assos?name=' + name) // Send GET request to server
      .then((res) => {
        setAsso(res.data);
        setLoading(false);
        if (res.data.Members.find((element) => (element.name === profile.profile.name && element.type === "admin")) !== null) {
          setAdminRights(true);
        } else {
          setAdminRights(false);
        }
      })
      .catch((err) => console.log(err));
  }, [name]);
  if(isLoading){
    return (<div>Loading</div>)
  }
  return (
    <div style={{ paddingLeft: "5vh", paddingRight: "5vh" }}>
      <Grid container spacing={1} sx={{ flexDirection: 'row',alignItems: "stretch"}}>
        <Grid item xs={3} zeroMinWidth>
          <h1
            style={{
              marginLeft: -30,
              textAlign: "center",
              fontSize: 25,
              color: theme.palette.text.primary
            }}
          >
            {asso["assoName"]}
          </h1>
          <Box
            component="div"
            ml={-5}
            sx={{
              height: "max-content",
              maxHeight: 250,
              maxWidth: 1440,
              //width: "90%",
              p: 2,
              overflow: "hidden",
              fontFamily: "Inter",
              border: 2,
              bgcolor: theme.palette.background.secondary,
              borderColor: theme.palette.background.secondary,
              color: theme.palette.text.secondary,
              borderRadius : 3
            }}
          >
            {asso["description"]}
          </Box>

          <text style={{ color: theme.palette.text.secondary }}></text>
        </Grid>
        <Grid item xs={9}>
          <NewsDisplay asso={asso} name={name} adminRights={adminRights}/>
        </Grid>
        <Grid item xs={3}>
          <MemberDisplay asso={asso} name={name} />
        </Grid>
        <Grid item xs={9}>
          <CalendarDisplay />
        </Grid>
      </Grid>
    </div>
  );
}

export default PageAsso;
