/*connexions.js*/

import React, { useState, useEffect } from 'react';
import { googleLogout, useGoogleLogin } from '@react-oauth/google';
import axios from 'axios';
import Grid from "@mui/material/Unstable_Grid2";

import {
    Paper,
    Typography,
    Grow,
    Box,
    Container,
    Button,
    useTheme,
    List,
    ListItem,
    ListItemText,
} from "@mui/material";
import { ThemeProvider, styled } from "@mui/material/styles";

import "@fontsource/inter";

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.tertiary,
    '&:hover': {
      backgroundColor: theme.palette.background.secondary,
    },
  }));

function Connexion({ profile, setProfile }) {
    const theme = useTheme();
    const [user, setUser] = useState([]);

    const login = useGoogleLogin({
        onSuccess: (codeResponse) => setUser(codeResponse),
        onError: (error) => console.log('Login Failed:', error)
    });


    useEffect(() => {
        // Check if profile data is available in local storage
        const storedProfile = localStorage.getItem('profile');
        if (storedProfile) {
            setProfile(JSON.parse(storedProfile));
        }
    }, [setProfile]);

    useEffect(() => {
        if (user) {
            axios
                .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${user.access_token}`, {
                    headers: {
                        Authorization: `Bearer ${user.access_token}`,
                        Accept: 'application/json'
                    }
                })
                .then((res) => {
                    const userProfile = res.data;
                    setProfile(userProfile);
                    // Store profile data in local storage
                    localStorage.setItem('profile', JSON.stringify(userProfile));
                    // Send a POST request to the server with user profile data
                    const dataToSend = {
                        username: userProfile.name,
                        email: userProfile.email
                    };

                    axios
                        .post('http://localhost:1234/users', dataToSend) // Send POST request to server
                        .then((res) => {
                            // Handle response from server if needed
                            console.log('User profile saved successfully');
                        })
                        .catch((err) => console.log(err));

                })
                .catch((err) => console.log(err));
        }
    }, [user, setProfile]);

    // log out function
    const logOut = () => {
        googleLogout();
        setProfile(null);
    };

    const newStyle = {
        width: "50%",
        color: theme.palette.primary.variant
    };

    return (
        <ThemeProvider theme={theme}>
            <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                <Grid item xs={12} >
                    <Typography
                        variant="h4"
                        noWrap
                        justifyContent={"center"}
                        sx={{
                            display: { xs: "flex", md: "flex" },
                            fontFamily: "Inter",
                            fontWeight: 1000,
                            fontSize: "2rem",
                            letterSpacing: ".001rem",
                            color: theme.palette.text.primary,
                        }}
                    >
                        Sign-in

                    </Typography>
                </Grid>
                {profile ? (
                    <Grid >
                        <Box textAlign='center' sx={{ mt: 4 }}>
                            <img src={profile.picture} alt="user image" style={{ width: 75, height: 75 }} />
                        </Box>

                        <Box display="flex" justifyContent="center" alignItems="center">
                            <Grid item xs={6} >
                                <List sx={{ color: theme.palette.text.primary }} >
                                    <ListItem disablePadding >
                                        <ListItemText primary="User Logged in" />
                                    </ListItem>
                                    <ListItem disablePadding >
                                        <ListItemText primary={"Nom et Prénom : " + profile.name} />
                                    </ListItem>
                                    <ListItem disablePadding>
                                        <ListItemText primary={"Adresse mail : " + profile.email} />
                                    </ListItem>
                                </List>
                            </Grid>
                        </Box>

                        <Box textAlign='center' sx={{ mt: 4 }}>
                            <ColorButton onClick={logOut} variant="contained"> Log out</ColorButton>
                        </Box>
                    </Grid>
                ) : (
                    <Box textAlign='center' sx={{ mt: 4 }}>
                        <ColorButton onClick={() => login()} variant="contained" > Sign in with Google</ColorButton>
                    </Box>
                )
                }
            </Container >
        </ThemeProvider >
    );
}
export default Connexion;