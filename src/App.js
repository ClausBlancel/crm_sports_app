import * as React from "react";
import { BrowserRouter, Route, Link, Routes, json } from "react-router-dom";
import {
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Typography,
  Menu,
  Container,
  Avatar,
  Button,
  Tooltip,
  MenuItem,
  Slide,
  useScrollTrigger,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";

import MenuIcon from "@mui/icons-material/Menu";
import AdbIcon from "@mui/icons-material/SportsMartialArts";

import { useTheme, ThemeProvider, createTheme } from "@mui/material";

import "@fontsource/inter";

import { GoogleOAuthProvider } from '@react-oauth/google';

import Home from "./pages/home";
import Connexion from "./pages/connexion";
import Account from "./pages/account";
import Dashboard from "./pages/dashboard";
import Pricing from "./pages/pricing";
import ListingAssociations from "./pages/listingAssociations";
import PageAsso from "./pages/pageAsso";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

import Footer from "./components/Footer/footer";



const ColorModeContext = React.createContext({ toggleColorMode: () => { } });

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

function App() {
  const [profile, setProfile] = React.useState(null);
  const pages = profile
    ? ["Liste des assos", "Pricing"]
    : ["Connexion", "Liste des assos", "Pricing"];
  const pathPages = profile
    ? [ "/associations", "/pricing"]
    : ["/Connexion", "/associations", "/pricing"];
  const settings = ["Log out", "Dashboard"];
  const pathSettings = ["/connexion", "/dashboard"];
  const theme = useTheme();
  const colorMode = React.useContext(ColorModeContext);
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };


  return (
    <Box sx={{ backgroundColor: theme.palette.background.primary, minHeight: "100vh", display: "flex", flexDirection: "column" }}>
      <BrowserRouter>
        <HideOnScroll>
          <AppBar elevation={0} position="sticky" style={{ backgroundColor: theme.palette.background.secondary }}>
            <Container maxWidth="xl" >
              <Toolbar disableGutters >
                <Link to="/" style={{ textDecoration: "none" }} sx={{ display: "inline-flex" }}>
                  <Grid container sx={{ display: { xs: 'none', md: 'flex' } }}>
                    <AdbIcon
                      sx={{
                        color: theme.palette.text.primary,
                        display: { xs: 'none', md: 'flex' },
                        mr: 1
                      }}
                    />
                    <Typography
                      variant="h5"
                      sx={{
                        mr: 2,
                        display: { xs: "none", md: "flex" },
                        fontFamily: "Inter",
                        fontWeight: 700,
                        letterSpacing: ".01rem",
                        color: theme.palette.text.primary,
                      }}
                    >
                      CRM Sport
                    </Typography>
                  </Grid>
                </Link>

                <Link to="/" sx={{ display: {xs: "inline-flex", md: "none" } }} style={{ textDecoration: "none", color: theme.palette.text.primary }}>
                  <Grid container textAlign={{ sx: "center" }} >
                    <AdbIcon
                      sx={{
                        color: theme.palette.text.primary,
                        display: { xs: "flex", md: "none" },
                        mr: 1,
                      }}
                    />
                    <Typography
                      variant="h5"
                      sx={{
                        mr: 2,
                        display: { xs: "flex", md: "none" },
                        fontFamily: "Inter",
                        fontWeight: 700,
                        letterSpacing: ".01rem",
                        color: theme.palette.text.primary,
                      }}
                    >
                      CRM Sport
                    </Typography>
                  </Grid>
                </Link>
                
                <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
                  <IconButton
                    size="large"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleOpenNavMenu}
                    sx={{ color: theme.palette.text.primary }}
                  >
                    <MenuIcon />
                  </IconButton>

                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorElNav}
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "left",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "left",
                    }}
                    open={Boolean(anchorElNav)}
                    onClose={handleCloseNavMenu}
                    sx={{
                      display: { xs: "block", md: "none" },
                    }}
                  >

                    {
                      pages.map((page) => (
                        <MenuItem
                          key={page}
                          component={Link}
                          to={pathPages[pages.indexOf(page)]}
                          onClick={handleCloseNavMenu}
                        >
                          <Typography
                            textAlign="center"
                            sx={{ fontFamily: "Inter", fontWeight: 300 }}
                          >
                            {page}
                          </Typography>
                        </MenuItem>
                      ))
                    }
                  </Menu>
                </Box>


                < Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                  {pages.map((page) => (
                    <Button
                      key={page}
                      onClick={handleCloseNavMenu}
                      sx={{
                        my: 2,
                        color: theme.palette.text.primary,
                        fontFamily: "Inter",
                        fontWeight: 300,
                        display: "block",
                      }}
                      component={Link}
                      to={pathPages[pages.indexOf(page)]}
                    >
                      {page}
                    </Button>
                  ))}
                </Box>

                <Box sx={{
                  flexGrow: 0,
                  display: {
                    xs: "flex",
                    md: "flex",
                    color: theme.palette.text.primary,
                  }
                }}
                >
                  <Tooltip title="Open settings">
                  {profile ? (
                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                        <Avatar alt={profile.name} src={profile.picture} />
                    </IconButton>
                      ) : (
                        <div></div>
                      )}
                  </Tooltip>
                  <Menu
                    sx={{ mt: "45px" }}
                    id="menu-appbar"
                    anchorEl={anchorElUser}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(anchorElUser)}
                    onClose={handleCloseUserMenu}
                  >
                    {settings.map((setting) => (
                      <MenuItem
                        key={setting}
                        component={Link}
                        to={pathSettings[settings.indexOf(setting)]}
                        onClick={handleCloseUserMenu}
                      >
                        <Typography textAlign="center">{setting}</Typography>
                      </MenuItem>
                    ))}
                  </Menu>

                  <IconButton
                    sx={{ ml: 1 }}
                    onClick={colorMode.toggleColorMode}
                    color="inherit"
                  >
                    {theme.palette.mode === "dark" ? (
                      <Brightness7Icon />
                    ) : (
                      <Brightness4Icon />
                    )}
                  </IconButton>
                </Box>
              </Toolbar>
            </Container>
          </AppBar>
        </HideOnScroll>

        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/connexion" element={<GoogleOAuthProvider clientId="20247603147-4ptukp22b62b9hmam6r9rut03h5rpu8a.apps.googleusercontent.com">
            <React.StrictMode>
              <Connexion profile={profile} setProfile={setProfile} />
            </React.StrictMode>
          </GoogleOAuthProvider>} />
          <Route path="/account" element={<Account />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/pricing" element={<Pricing />} />
          <Route path="/associations" element={<ListingAssociations />} />
          <Route path="/association/:name" element={<PageAsso profile={profile} />} />
        </Routes>

        <Footer />
      </BrowserRouter>
    </Box >
  );
}

export default function ToggleColorMode() {
  const [mode, setMode] = React.useState('light');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [setMode],
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        breakpoints: {
          values: {
            xs: 0,
            sm: 600,
            md: 1000,
            lg: 1200,
            xl: 1536,
          },
        },
        palette: {
          mode,
          ...(mode === 'light'
            ? {
              // palette values for light mode
              general: {
                main: "#0A2463",
              },
              text: {
                primary: "#16166b",
                secondary: "#3C3D45",
                tertiary: "#ffffff"
              },
              background: {
                primary: "#FFFFFF",
                secondary: "#f8f8ff",
                tertiary: "#9C9CD9",
              },
            }
            : {
              // palette values for dark mode
              general: {
                main: "#0A2463",
              },
              text: {
                primary: "#FFFFFF",
                secondary: "#f8f8ff",
                tertiary: "#ffffff",
              },
              background: {
                primary: "#2e2e38",
                secondary: "#111111",
                tertiary: "#27276D",
              },
            }),
        },
      }),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};