import * as React from "react";
import {
    Box,
    Typography,
    Container,
    Link,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";


import { useTheme} from "@mui/material";

import "@fontsource/inter";


const footers = [
    {
        title: "Company",
        description: ["Team", "History", "Contact us", "Locations"]
    },
    {
        title: "Features",
        description: [
            "Cool stuff",
            "Random feature",
            "Team feature",
            "Developer stuff",
            "Another one"
        ]
    },
    {
        title: "Resources",
        description: [
            "Resource",
            "Resource name",
            "Another resource",
            "Final resource"
        ]
    },
    {
        title: "Legal",
        description: ["Privacy policy", "Terms of use"]
    }
];


export default function Footer() {
    const theme = useTheme();

    return (
        <>
            <Box sx={{ height:"50px" }}></Box>
            <Box sx={{ backgroundColor: theme.palette.background.secondary, marginTop: "auto" }}>
                <Container
                    maxWidth="md"
                    component="footer"
                    sx={{
                        borderTop: (theme) => `1px solid ${theme.palette.divider}`,
                        py: [3, 6]
                    }}
                >
                    <Grid container spacing={4} justifyContent="space-evenly">
                        {footers.map((footer) => (
                            <Grid item xs={6} sm={3} key={footer.title}>
                                <Typography variant="h6" sx={{ color: theme.palette.text.primary }} gutterBottom>
                                    {footer.title}
                                </Typography>
                                <ul>
                                    {footer.description.map((item) => (
                                        <li key={item}>
                                            <Link
                                                href="#"
                                                variant="subtitle1"
                                                color="text.secondary"
                                            >
                                                {item}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </Grid>
                        ))}
                    </Grid>
                    <Copyright sx={{ mt: 5 }} />
                </Container>
            </Box>
        </>
    )
}


function Copyright(props) {
    return (
        <Typography
            variant="body2"
            color="text.secondary"
            align="center"
            {...props}
        >
            {"Copyright © "}
            <Link color="inherit">CRM Sport</Link> {new Date().getFullYear()}
            {"."}
        </Typography>
    );
}